Unit GLCompImages;

{ FCL Compact Image OpenGL Interface (c) 2016, Paul F. Michell. }

{$IFDEF FPC}
  {$ASMMODE INTEL}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

{$I Defines.inc}

Uses
  Classes, GL, GLU, GLExt, CompImages;

{$IFDEF BGRAIMAGE}
Const
  GL_BGRA_EXT = $80E1;
{$ENDIF}

Type
  TGLCompactImage = Class
  Private
    FCompactImage: TCompactImage;
  Public
    Constructor Create(CompactImage: TCompactImage);
    Destructor Destroy; Override;
    Procedure DrawToOpenGL(DisplayRect: TRect);
  End;

Implementation

Constructor TGLCompactImage.Create(CompactImage: TCompactImage);
Begin
  FCompactImage := CompactImage;
End;

Destructor TGLCompactImage.Destroy;
Begin
  Inherited Destroy;
End;

Var
  cube_rotationx: GLFloat;
  cube_rotationy: GLFloat;
  cube_rotationz: GLFloat;

Procedure TGLCompactImage.DrawToOpenGL(DisplayRect: TRect);
//Var
//  Texture: GLUInt;
Begin
  glClearColor(0.0, 0.0, 0.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  glOrtho(0, FCompactImage.Width, 0, FCompactImage.Height, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  glDisable(GL_DEPTH_TEST);
  glViewport(DisplayRect.Left, DisplayRect.Bottom, DisplayRect.Right, DisplayRect.Top);
  {$IFDEF BGRAIMAGE}
  glDrawPixels(FCompactImage.Width, FCompactImage.Height, GL_BGRA_EXT, GL_UNSIGNED_BYTE, FCompactImage.Data);
  {$ELSE}
  glDrawPixels(FCompactImage.Width, FCompactImage.Height, GL_RGBA, GL_UNSIGNED_BYTE, FCompactImage.Data);
  {$ENDIF}
  glRasterPos2i(0, FCompactImage.Height);
  glPixelZoom(1.0, -1.0);
{  glClearColor(1.0, 0.0, 0.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT Or GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  glOrtho(0, FCompactImage.Width, FCompactImage.Height, 0, 0, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glDisable(GL_DEPTH_TEST);
  glViewport(0, 0, FCompactImage.Width, FCompactImage.Height);
  glGenTextures(1, @Texture);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, Texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_RECTANGLE_EXT, 0, GL_RGBA, FCompactImage.Width, FCompactImage.Height,
               0, GL_RGBA, GL_UNSIGNED_BYTE, FCompactImage.Data);

  glEnable(GL_TEXTURE_RECTANGLE);
  glBegin(GL_QUADS);
    glTexCoord2i(0, 0);
    glVertex3i(0, 0, 0);
    glTexCoord2i(FCompactImage.Width, 0);
    glVertex3i(FCompactImage.Width, 0, 0);
    glTexCoord2i(FCompactImage.Width, FCompactImage.Height);
    glVertex3i(FCompactImage.Width, FCompactImage.Height, 0);
    glTexCoord2i(0, FCompactImage.Height);
    glVertex3i(0, FCompactImage.Height, 0);
  glEnd;
  glDisable(GL_TEXTURE_RECTANGLE);
  glDeleteTextures(1, @Texture);
  glFinish;
}
  {  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  glOrtho(0.0, 100, 100, 0.0, -1.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  glGenTextures(1, @Texture);
  glBindTexture(GL_TEXTURE_2D, Texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
               FCompactImage.Width, FCompactImage.Height, 0,
               GL_RGBA, GL_UNSIGNED_BYTE, FCompactImage.Data);
  glDisable(GL_CULL_FACE);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glDisable(GL_BLEND);
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
  glColor3f(1.0, 1.0, 1.0);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex2f(0.0, 0.0);
  glTexCoord2f(100, 0.0);
  glVertex2f(FCompactImage.Width, 0.0);
  glTexCoord2f(100, 100);
  glVertex2f(FCompactImage.Width, FCompactImage.Height);
  glTexCoord2f(0.0, 100);
  glVertex2f(0.0, FCompactImage.Height);
  glEnd;
}
End;

End.

