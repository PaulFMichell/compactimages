Unit CompFonts;

{ FCL Compact Image Font Rendering Unit (c) 2016, Paul F. Michell. }

{$IFDEF FPC}
  {$ASMMODE INTEL}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, SysUtils, CompImages, Types, Math, fpFreeType;

Type
  TCompactFont = Class
  Private
    FImage: TCompactImage;
    FFreeTypeFont: TFreeTypeFont;
    Procedure DirectRenderer(x, y, tx: Integer; SourceData: Pointer);
  Public
    TextColor: TCompactColor;
    Constructor Create(Image: TCompactImage);
    Destructor Destroy; Override;
    Function LoadFromFile(FileName: String): Boolean;
    Function LoadFromStream(Stream: TStream): Boolean;
    Procedure TextRect(Text: String; Rect: TRect);
  End;

Implementation

Const
  GammaExponentFactor   = 1.7;

Var
  GammaLinearFactor: single;
  GammaExpansionTable: Packed Array[0..255] Of Word;
  GammaCompressionTable: Packed Array[0..65535] Of Byte;

Procedure BuildGammaTables;
Var
  i: integer;
Begin
  { The linear factor is used to normalize expanded values in the range 0..65535. }
  GammaLinearFactor := 65535/Power(255, GammaExponentFactor);
  For i := 0 To 255 Do
    GammaExpansionTable[i] := Round(Power(i, GammaExponentFactor)*GammaLinearFactor);
  For i := 0 To 65535 Do
    GammaCompressionTable[i] := Round(Power(i/GammaLinearFactor, 1/GammaExponentFactor));
  GammaExpansionTable[1] := 1;  { To avoid information loss. }
  GammaCompressionTable[1] := 1;
End;

Constructor TCompactFont.Create(Image: TCompactImage);
Begin
  FImage := Image;
  BuildGammaTables;
  FFreeTypeFont := TFreeTypeFont.Create;
  FFreeTypeFont.ClearType := False;
End;

Destructor TCompactFont.Destroy;
Begin
  Inherited Destroy;
  FFreeTypeFont.Free;
End;

Function TCompactFont.LoadFromFile(FileName: String): Boolean;
Begin
  FFreeTypeFont.Name := FileName;
End;

Function TCompactFont.LoadFromStream(Stream: TStream): Boolean;
Begin
  FFreeTypeFont.AccessFromStream(Stream, False);
End;

Procedure TCompactFont.DirectRenderer(x, y, tx: Integer; SourceData: Pointer);
Var
  psrc: pbyte;
  dest: TCompactColorPointer;
  p: PByte;
  a1f, a2f, a12, a12m: cardinal;
  c: TCompactColor;
Begin
  With FImage Do
    If (y>=0) And (y<Height) And (x>=0) And (x<Width-tx) Then
      Begin
        psrc := pbyte(SourceData);
        dest := Data;
        Inc(dest, (y*Width)+x);
        While tx>0 Do
          Begin
            c := TextColor;
            c.Alpha := c.Alpha*(psrc^+1) ShR 8;
            If c.Alpha>0 Then
              If c.Alpha = 255 Then
                dest^ := c
              Else
                Begin
                  {$HINTS OFF}
                  a12 := 65025-(Not dest^.Alpha)*(Not c.Alpha);
                  {$HINTS ON}
                  a12m := a12 ShR 1;
                  a1f := dest^.Alpha*(Not c.Alpha);
                  a2f := (c.Alpha ShL 8)-c.Alpha;
                  p := PByte(dest);
                  {$IFDEF WINDOWS}
                  p^ := GammaCompressionTable[(GammaExpansionTable[dest^.Blue]*a1f+
                        GammaExpansionTable[c.Blue]*a2f+a12m) Div a12];
                  Inc(p);
                  p^ := GammaCompressionTable[(GammaExpansionTable[dest^.Green]*a1f+
                        GammaExpansionTable[c.Green]*a2f+a12m) Div a12];
                  Inc(p);
                  p^ := GammaCompressionTable[(GammaExpansionTable[dest^.Red]*a1f+
                        GammaExpansionTable[c.Red]*a2f+a12m) Div a12];
                  {$ENDIF}
                  {$IFDEF UNIX}
                  p^ := GammaCompressionTable[(GammaExpansionTable[dest^.Red]*a1f+
                        GammaExpansionTable[c.Red]*a2f+a12m) Div a12];
                  Inc(p);
                  p^ := GammaCompressionTable[(GammaExpansionTable[dest^.Green]*a1f+
                        GammaExpansionTable[c.Green]*a2f+a12m) Div a12];
                  Inc(p);
                  p^ := GammaCompressionTable[(GammaExpansionTable[dest^.Blue]*a1f+
                        GammaExpansionTable[c.Blue]*a2f+a12m) Div a12];
                  {$ENDIF}
                  Inc(p);
                  p^ := (a12+a12 ShR 7) ShR 8;
                End;
            Inc(psrc);
            Inc(dest);
            Dec(tx);
          End;
      End;
End;

Procedure TCompactFont.TextRect(Text: String; Rect: TRect);
Var
  DefaultWidth, DefaultHeight, NormalWidth: Single;
  TextStart: Single;
Begin
  FFreeTypeFont.WidthFactor := 1;
  DefaultHeight := Rect.Bottom-Rect.Top;
  DefaultWidth := Rect.Right-Rect.Left;
  FFreeTypeFont.LineFullHeight := DefaultHeight;
  NormalWidth := FFreeTypeFont.TextWidth(Text);
  If NormalWidth>DefaultWidth Then
    Begin
      FFreeTypeFont.WidthFactor := DefaultWidth/NormalWidth;
      TextStart := 0;
    End
  Else
    TextStart := Trunc((DefaultWidth-NormalWidth)/2);
  FFreeTypeFont.RenderText(Text, Rect.Left+TextStart, Rect.Top+FFreeTypeFont.Ascent, Rect, @DirectRenderer);
End;

End.

