Unit Main;

{ FCL Compact Image Test Main Form (c) 2016, Paul F. Michell. }

{$IFDEF FPC}
  {$ASMMODE INTEL}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  Classes, SysUtils, Forms, Dialogs, Controls, ActnList, Graphics, Menus,
  StdCtrls, ExtCtrls, OpenGLContext, GraphType, CompImages, CompFonts,
  LCLCompImages, GLCompImages;

Type
  TMainForm = Class(TForm)
    UseOpenGLMenuItem: TMenuItem;
    UseOpenGLAction: TAction;
    LoadImageAction: TAction;
    MainMenu: TMainMenu;
    FileMenuItem: TMenuItem;
    ExitMenuItem: TMenuItem;
    EditMenuItem: TMenuItem;
    FullscreenMenuItem: TMenuItem;
    LoadMenuItem: TMenuItem;
    InfoMenuItem: TMenuItem;
    BenchmarkMenuItem: TMenuItem;
    OpenDialog: TOpenDialog;
    OpenGLControl: TOpenGLControl;
    SaveDialog: TSaveDialog;
    SaveMenuItem: TMenuItem;
    SaveImageAction: TAction;
    ExitAction: TAction;
    FullscreenAction: TAction;
    InfoAction: TAction;
    BenchmarkAction: TAction;
    ActionList: TActionList;
    Procedure ExitActionExecute(Sender: TObject);
    Procedure FormCreate(Sender: TObject);
    Procedure FormDestroy(Sender: TObject);
    Procedure FormPaint(Sender: TObject);
    Procedure FullscreenActionExecute(Sender: TObject);
    Procedure InfoActionExecute(Sender: TObject);
    Procedure LoadImageActionExecute(Sender: TObject);
    Procedure BenchmarkActionExecute(Sender: TObject);
    Procedure OpenGLControlPaint(Sender: TObject);
    Procedure OpenGLControlResize(Sender: TObject);
    Procedure SaveImageActionExecute(Sender: TObject);
    Procedure UseOpenGLActionExecute(Sender: TObject);
  Private
    BackgroundColor: TCompactColor;
    TestImage: TCompactImage;
    TestLCLImage: TLCLCompactImage;
    TestGLImage: TGLCompactImage;
    TestFont: TCompactFont;
  End;

Var
  MainForm: TMainForm;

Implementation

{$R *.lfm}

Procedure TMainForm.FormCreate(Sender: TObject);
Var
  FontPath: String;
Begin
  With BackgroundColor Do
    Begin
      Red := 0;
      Green := 0;
      Blue := 0;
      Alpha := 255;
    End;
  TestImage := TCompactImage.Create(Screen.Width, Screen.Height);
  TestLCLImage := TLCLCompactImage.Create(TestImage);
  TestGLImage := TGLCompactImage.Create(TestImage);
  TestImage.Clear(BackgroundColor);
  TestFont := TCompactFont.Create(TestImage);
  FontPath := ExtractFilePath(ParamStr(0));
  { Sawasdee Bold Font File
    Copyright: 2007 Pol Udomwittayanukul <webnaipol@gmail.com>
    License: GPL-2+ with Font exception. }
  TestFont.LoadFromFile(FontPath+'Sawasdee-Bold.ttf');
  With TestFont.TextColor Do
    Begin
      Red := 255;
      Green := 255;
      Blue := 255;
      Alpha := 255;
    End;
End;

Procedure TMainForm.FormDestroy(Sender: TObject);
Begin
  If OpenGLControl.Visible Then
    OpenGLControl.Hide;
  TestFont.Free;
  TestGLImage.Free;
  TestLCLImage.Free;
  TestImage.Free;
End;

Procedure TMainForm.FormPaint(Sender: TObject);
Begin
  If Not UseOpenGLAction.Checked Then
    TestLCLImage.DrawTo(Canvas);
End;

Procedure TMainForm.ExitActionExecute(Sender: TObject);
Begin
  Close;
End;

Var
  WindowRect: TRect;

Procedure TMainForm.FullscreenActionExecute(Sender: TObject);
Begin
  Begin
    If FullscreenAction.Checked Then
      Begin
        WindowRect := BoundsRect;
        BorderStyle := bsNone;
        WindowState := wsFullScreen;
        {$IFNDEF DARWIN}Menu := Nil;{$ENDIF}
      End
    Else
      Begin
        BorderStyle := bsSizeable;
        WindowState := wsMaximized;
        {$IFNDEF DARWIN}Menu := MainMenu;{$ENDIF}
        WindowState := wsNormal;
        BoundsRect := WindowRect;
      End;
  End;
End;

Procedure TMainForm.InfoActionExecute(Sender: TObject);
Begin
  ShowMessage(TestLCLImage.Description);
End;

Procedure TMainForm.LoadImageActionExecute(Sender: TObject);
Var
  StartTime: TDateTime;
  LoadTime: TDateTime;
Begin
  With OpenDialog Do
    If Execute Then
      Begin
        StartTime := Now();
        TestImage.LoadFromFile(Filename);
        LoadTime := Now()-StartTime;
        TestLCLImage.Update;
        Repaint;
        ShowMessage('Fast Bitmap Load Time: '+FormatFloat('0.00', MSecsPerDay*LoadTime)+'ms');
      End;
End;

Procedure TMainForm.SaveImageActionExecute(Sender: TObject);
Begin
  With SaveDialog Do
    If Execute Then
      Begin
        TestImage.SaveToFile(Filename);
        ShowMessage('Test image saved.');
      End;
End;

Procedure TMainForm.UseOpenGLActionExecute(Sender: TObject);
Begin
  OpenGLControl.Visible := UseOpenGLAction.Checked;
  Repaint;
End;

Procedure TMainForm.BenchmarkActionExecute(Sender: TObject);
Var
  StartTime: TDateTime;
  DrawTime: TDateTime;
  Index: Integer;
Begin
  StartTime := Now();
  For Index := 0 to 255 do
    Begin
      BackgroundColor.Red := Index;
      TestImage.Clear(BackgroundColor);
      TestFont.TextRect(IntToStr(Index), ClientRect);
      Repaint;
      Application.ProcessMessages;
     End;
  DrawTime := Now()-StartTime;
  ShowMessage('Fast Bitmap Average Draw Time: '+FormatFloat('0.00', MSecsPerDay*DrawTime/256)+
              'ms ('+FormatFloat('0.00', 256/(SecsPerDay*DrawTime))+'fps)');
End;

Procedure TMainForm.OpenGLControlPaint(Sender: TObject);
Begin
  If UseOpenGLAction.Checked Then
    Begin
      OpenGLControl.MakeCurrent;
      TestGLImage.DrawToOpenGL(OpenGLControl.ClientRect);
      OpenGLControl.SwapBuffers;
    End;
End;

Procedure TMainForm.OpenGLControlResize(Sender: TObject);
Begin
//  If UseOpenGLAction.Checked Then
//    OpenGLControl.Repaint;
End;

End.

