Unit LCLCompImages;

{ FCL Compact Image LCL Interface (c) 2016, Paul F. Michell. }

{$IFDEF FPC}
  {$ASMMODE INTEL}
  {$MODE OBJFPC}
  {$LONGSTRINGS ON}
{$ENDIF}

Interface

Uses
  SysUtils, {$IFDEF WINDOWS}Windows,{$ENDIF}{$IFDEF LCLGTK2}GDK2, Gtk2Def,{$ENDIF}
  {$IFDEF LCLQT}QT4, QTWidgets, QTObjects,{$ENDIF}{$IFDEF LCLCARBON}MacOSAll, CarbonCanvas,{$ENDIF}
  Graphics, GraphType, Types, CompImages;

{$I Defines.inc}

Type
  TLCLCompactImage = Class
  Private
    FCompactImage: TCompactImage;
    FImageBitmap: TBitmap;
    FRawImage: TRawImage;
    Function GetImageBitmap: TBitmap;
  Public
    Constructor Create(CompactImage: TCompactImage);
    Destructor Destroy; Override;
    Function Description: String;
    Procedure DrawTo(Canvas: TCanvas);
    Procedure Update;
    Property ImageBitmap: TBitmap Read GetImageBitmap;
    Property RawImage: TRawImage Read FRawImage;
  End;

Implementation

Constructor TLCLCompactImage.Create(CompactImage: TCompactImage);
Begin
  FCompactImage := CompactImage;
  FImageBitmap := TBitmap.Create;
  Update;
End;

Destructor TLCLCompactImage.Destroy;
Begin
  FImageBitmap.Free;
  Inherited Destroy;
End;

Function TLCLCompactImage.Description: String;
Begin
  Result := StringReplace(FRawImage.Description.AsString, ' ', LineEnding, [rfReplaceAll]);
End;

Procedure TLCLCompactImage.DrawTo(Canvas: TCanvas);
{$IFDEF WINDOWS}
Var
  BitsInfo: TBitmapInfo;
Begin
  With BitsInfo.bmiHeader Do
    Begin
      biSize := SizeOf(TBitmapInfoHeader);
      biWidth := FCompactImage.Width;
      biHeight := -FCompactImage.Height; { Note: negative height flags image as bottom to top scanline order. }
      biPlanes := 1;
      biBitCount := FRawImage.Description.BitsPerPixel;
      biCompression := BI_RGB;
      biSizeImage :=  FRawImage.DataSize;
      biXPelsPerMeter := 0;
      biYPelsPerMeter := 0;
      biClrUsed := 0;
      biClrImportant := 0;
    End;
  Canvas.Lock;
  StretchDIBits(Canvas.Handle, 0, 0, FCompactImage.Width, FCompactImage.Height,
                0, 0, FCompactImage.Width, FCompactImage.Height, FRawImage.Data,
                BitsInfo, DIB_RGB_COLORS, SRCCOPY);
  Canvas.Unlock;
End;
{$ELSE}{$IFDEF LCLGTK2}
Begin
  gdk_draw_rgb_32_image(TGTKDeviceContext(Canvas.Handle).Drawable,
                        TGTKDeviceContext(Canvas.Handle).GC, 0, 0,
                        FCompactImage.Width, FCompactImage.Height,
                        GDK_RGB_DITHER_NORMAL,
                        FRawImage.Data, FRawImage.Description.BytesPerLine);
End;

{$ELSE}{$IFDEF LCLQT}
Var
  QtImage: TQtImage;
  QtContext: TQtDeviceContext;
  Rect: TRect;
Begin
  QtImage := TQtImage.Create(FRawImage.Data, FCompactImage.Width, FCompactImage.Height, QImageFormat_ARGB32);
  Rect := Types.Rect(0, 0, FCompactImage.Width, FCompactImage.Height);
  QtContext := TQtDeviceContext(Canvas.Handle);
  QtContext.drawImage(@Rect, QtImage.Handle, @Rect, Nil, @Rect);
  QtImage.Free;
End;
{$ELSE}{$IFDEF LCLCARBON}
Var
  Image: CGImageRef;
  BitmapReference: CGContextRef;
Begin
  BitmapReference := CGBitmapContextCreate(FRawImage.Data, FCompactImage.Width, FCompactImage.Height,
                                           8, 4*FCompactImage.Width,
                                           CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB),
                                           kCGImageAlphaPremultipliedLast);
  Image := CGBitmapContextCreateImage(BitmapReference);
  TCarbonDeviceContext(Canvas.Handle).DrawCGImage(0, 0, FCompactImage.Width, FCompactImage.Height, Image);
  CGImageRelease(Image);
  CGContextRelease(BitmapReference);
End;
{$ELSE}
Begin
  GetImageBitmap;
  Canvas.Lock;
  Canvas.Draw(0, 0, ImageBitmap);
  Canvas.Unlock;
End;
{$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}

Procedure TLCLCompactImage.Update;
Begin
  {$IFDEF BGRAIMAGE}
  FRawImage.Description.Init_BPP32_B8G8R8A8_BIO_TTB(FCompactImage.Width, FCompactImage.Height);
  {$ELSE}
  FRawImage.Description.Init_BPP32_R8G8B8A8_BIO_TTB(FCompactImage.Width, FCompactImage.Height);
  {$ENDIF}
  FRawImage.Data := PByte(FCompactImage.Data);
  FRawImage.DataSize := FRawImage.Description.BytesPerLine*FCompactImage.Height;
End;

Function TLCLCompactImage.GetImageBitmap: TBitmap;
Begin
  FImageBitmap.LoadFromRawImage(FRawImage, False);
  Result := FImageBitmap;
End;

End.

